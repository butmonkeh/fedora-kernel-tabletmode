# fedora-kernel+tabletmode



A build of https://src.fedoraproject.org/rpms/kernel.git with the amd-tablet-sfh.patch added, for Fedora 41.

Download, `cd` into directory then `# dnf install ./kernel*.rpm`

*** NOTICE ***
From 6.10.3 kernel prevents the dGPU from suspending on my GV301 (Nvidia GTX1650), but ymmv.

To fix this, set 0x02 as per :
 Most Nvidia laptops support different levels of runtime power management that can prolong battery life. These have been buggy in some use cases, so they are not set by default. To configure these, copy this text into a file in /etc/modprobe.d/ and modify it to meet your needs.
```
# 0x00 - no power management, 0x01 - coarse power management, 0x02 - fine power management
options nvidia NVreg_DynamicPowerManagement=0x02
```
.